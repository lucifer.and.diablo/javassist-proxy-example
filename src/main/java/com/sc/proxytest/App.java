/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.proxytest;

import java.io.File;
import java.net.URL;
import javassist.ByteArrayClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cglib.core.ReflectUtils;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;

/**
 *
 * @author lucifer
 */
@SpringBootConfiguration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class App {

    public static ClassPool cp;

    static {
        try {
            String javaHome = System.getProperty("java.home");
            String lib = javaHome + File.separator + (javaHome.contains("jre") ? ".." + File.separator + "lib" + File.separator + "tools.jar" : "lib" + File.separator + "tools.jar");
            URL toolsPath = new File(lib).toURI().toURL();
            Utils.addURL(toolsPath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void defineClass(String name, byte[] define) {
        try {
            cp.insertClassPath(new ByteArrayClassPath(name, define));
            cp.get(name).writeFile("exported");
        } catch (Exception e) {
        }
    }

    private static void injectCodeDefenition() throws Exception {

        CtClass ct = cp.get(ReflectUtils.class.getName());
        CtMethod[] m = ct.getDeclaredMethods("defineClass");
        for (CtMethod c : m) {
            if (c.getParameterTypes().length < 2) {
                continue;
            }
            if (!c.getParameterTypes()[1].isArray()) {
                continue;
            }
            c.insertBefore("if ($1.contains(\"CGLIB\")) { com.sc.proxytest.App.defineClass($1,$2); }");
        }

        ModificationHelper.redefine(ReflectUtils.class, ct);
    }

    @Bean
    public TestBean testBean() {
        return new TestBean();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        PlatformTransactionManager ptm = new AbstractPlatformTransactionManager() {
            private Object transaction;

            @Override
            protected Object doGetTransaction() throws TransactionException {
                return transaction;
            }

            @Override
            protected void doBegin(Object transaction, TransactionDefinition definition) throws TransactionException {
                this.transaction = transaction;
            }

            @Override
            protected void doCommit(DefaultTransactionStatus status) throws TransactionException {
                transaction = null;
            }

            @Override
            protected void doRollback(DefaultTransactionStatus status) throws TransactionException {
                transaction = null;
            }
        };
        return ptm;
    }

    public static void main(String[] args) throws Exception {
        cp = ClassPool.getDefault();
//        cp.insertClassPath(new ClassClassPath(App.class));
        Thread.currentThread().setContextClassLoader(cp.getClassLoader());

        injectCodeDefenition();

        ConfigurableApplicationContext ctx = SpringApplication.run(App.class, args);
        TestBean tb = ctx.getBean(TestBean.class);

        tb.c();
        tb.a();
    }
}
