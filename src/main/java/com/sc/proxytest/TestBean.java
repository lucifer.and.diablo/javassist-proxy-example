/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.proxytest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 *
 * @author lucifer
 */
public class TestBean {

    public void a() {
        b();
    }

    public void b() {
        c();
    }

    @Transactional
    public void c() {
        TransactionStatus status = TransactionAspectSupport.currentTransactionStatus();
        System.out.println(status);
    }
}
